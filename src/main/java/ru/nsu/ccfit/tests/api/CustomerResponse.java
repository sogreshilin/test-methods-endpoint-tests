package ru.nsu.ccfit.tests.api;


public class CustomerResponse {
    public String id;
    public String firstName;
    public String lastName;
    public String login;
    public String pass;
    public int balance;

    @Override
    public String toString() {
        return "Customer{" +
            "id='" + id + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", login='" + login + '\'' +
            ", pass='" + pass + '\'' +
            ", balance=" + balance +
            '}';
    }
}

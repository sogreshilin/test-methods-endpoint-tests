package ru.nsu.ccfit.tests.api;


public class PlanRequest {
    public String id;
    public String name;
    public String details;
    public int fee;

    @Override
    public String toString() {
        return "Plan{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", details='" + details + '\'' +
            ", fee=" + fee +
            '}';
    }
}

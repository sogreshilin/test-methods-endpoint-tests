package ru.nsu.ccfit.tests.api;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

public final class Utils {
    private final static String URI = "http://localhost:8080/endpoint/rest";

    private final Logger log = LoggerFactory.getLogger(Utils.class);
    private Client client;

    public Utils() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        client = ClientBuilder.newClient(clientConfig);
    }

    public String get(String path, Map<String, Object> queryParams) {
        WebTarget webTarget = target().path(path);
        for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
            webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        log("GET " + URI + "/" + path + " " + queryParams);
        Response response = invocationBuilder.get();
        String responseJson = response.readEntity(String.class);
        log("Response body:\n" + responseJson);
        return responseJson;
    }

    public String post(String path, String requestBody) {
        WebTarget webTarget = target().path(path);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        log("POST " + URI + "/" + path + "\n" + requestBody);
        Response response = invocationBuilder.post(Entity.entity(requestBody, MediaType.APPLICATION_JSON));
        String responseJson = response.readEntity(String.class);
        log("Response body: " + responseJson);
        return responseJson;
    }

    public String put(String path, String requestBody) {
        WebTarget webTarget = target().path(path);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        log("PUT " + URI + "/" + path + "\n" + requestBody);
        Response response = invocationBuilder.put(Entity.entity(requestBody, MediaType.APPLICATION_JSON));
        String responseJson = response.readEntity(String.class);
        log("Response body: " + responseJson);
        return responseJson;
    }

    public String delete(String path, Map<String, Object> queryParams) {
        WebTarget webTarget = target().path(path);
        for (Map.Entry<String, Object> entry : queryParams.entrySet()) {
            webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
        }
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        log("DELETE " + URI + "/" + path + " " + queryParams);
        Response response = invocationBuilder.delete();
        String responseJson = response.readEntity(String.class);
        log("Response body: " + responseJson);
        return responseJson;
    }

    private void log(String logString) {
        log.info(logString);
        Reporter.log(logString);
    }

    private WebTarget target() {
        return client.target(URI);
    }
}

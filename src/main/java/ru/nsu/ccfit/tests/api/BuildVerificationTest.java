package ru.nsu.ccfit.tests.api;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BuildVerificationTest {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Utils utils = new Utils();

    private String login;
    private String planName;
    private String customerId;
    private String planId;

    @Test(description = "Create new customer")
    public void createCustomer() throws IOException {
        login = UUID.randomUUID() + "@gmail.com";
        CustomerRequest customerRequest = createDefaultCustomerRequest();
        customerRequest.login = login;
        String requestJson = objectMapper.writeValueAsString(customerRequest);

        String responseJson = utils.post("customers", requestJson);

        CustomerResponse customerResponse = objectMapper.readValue(responseJson, CustomerResponse.class);
        customerId = customerResponse.id;
        Assert.assertNotNull(customerId);
    }

    @Test(description = "Get customer by login", dependsOnMethods = "createCustomer")
    public void getCustomerByLogin() throws IOException {
        String rawResponse = utils.get("customers", ImmutableMap.of("login", login));

        List<CustomerResponse> customers = objectMapper.readValue(
            rawResponse,
            new TypeReference<List<CustomerResponse>>() {
            }
        );
        Assert.assertEquals(customers.size(), 1);
        Assert.assertEquals(customers.get(0).login, login);
    }

    @Test(description = "Create new plan")
    public void createPlan() throws IOException {
        planName = UUID.randomUUID().toString();
        PlanRequest planRequest = createDefaultPlanRequest();
        planRequest.name = planName;
        String requestJson = objectMapper.writeValueAsString(planRequest);

        String responseJson = utils.post("plans", requestJson);

        PlanResponse planResponse = objectMapper.readValue(responseJson, PlanResponse.class);
        planId = planResponse.id;
    }

    @Test(description = "Update plan", dependsOnMethods = "createPlan")
    public void updatePlan() throws IOException {
        planName = UUID.randomUUID().toString();
        PlanRequest planRequest = createDefaultPlanRequest();
        planRequest.id = planId;
        planRequest.name = planName;
        String requestJson = objectMapper.writeValueAsString(planRequest);

        String responseJson = utils.put("plans", requestJson);

        PlanResponse planResponse = objectMapper.readValue(responseJson, PlanResponse.class);
        planId = planResponse.id;
        planName = planResponse.name;
    }

    @Test(description = "Get plans available for purchase", dependsOnMethods = {"createPlan", "createCustomer"})
    public void getPlansAvailableForPurchase() throws IOException {
        String responseJson = utils.get("plans", ImmutableMap.of("customerId", customerId));
        Assert.assertEquals(responseJson, "[ ]");
    }

    @Test(description = "Delete plan", dependsOnMethods = "updatePlan")
    public void deletePlan() throws IOException {
        String responseJson = utils.delete("plans", ImmutableMap.of("planId", planId));
        Assert.assertEquals(responseJson, "{\"status\":\"OK\"}");
    }

    private PlanRequest createDefaultPlanRequest() {
        PlanRequest planRequest = new PlanRequest();
        planRequest.name = "default plan name";
        planRequest.details = "default plan details";
        planRequest.fee = 100;
        return planRequest;
    }

    private CustomerRequest createDefaultCustomerRequest() {
        CustomerRequest customerRequest = new CustomerRequest();
        customerRequest.firstName = "Alexander";
        customerRequest.lastName = "Sogreshilin";
        customerRequest.login = "sogreshilin";
        customerRequest.pass = "password123";
        customerRequest.balance = 0;
        return customerRequest;
    }
}

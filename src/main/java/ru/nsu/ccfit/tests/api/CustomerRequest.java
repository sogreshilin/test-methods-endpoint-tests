package ru.nsu.ccfit.tests.api;


public class CustomerRequest {
    public String firstName;
    public String lastName;
    public String login;
    public String pass;
    public int balance;

    @Override
    public String toString() {
        return "Customer{" +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", login='" + login + '\'' +
            ", pass='" + pass + '\'' +
            ", balance=" + balance +
            '}';
    }
}
